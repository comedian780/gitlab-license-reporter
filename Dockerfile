FROM alpine

WORKDIR /reporter
COPY . .

RUN apk add --no-cache jq

ENTRYPOINT [ "ash", "licensereport.sh" ]
