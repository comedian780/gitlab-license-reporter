.licenses[]
  | select(
            .name | contains (
                                  "GPL", "MPL", "EPL", "OSL", "MS-RL", "ODBL", "RPL", "PPL", "NGPL", "RPSL", "OCLC", "Mechanical"
                             )
          )
