# license-reporter
Container to scan Gitlab license scan reports for licenses with a copyleft.  

Usage:

```sh
# without name changes (defaults to gl-license-management-report.json)
docker run -it -v ${PWD}:/reports registry.gitlab.com/comedian780/gitlab-license-reporter/license-reporter

# with custom report names
docker run -it -v ${PWD}/special_report.json registry.gitlab.com/comedian780/gitlab-license-reporter/license-reporter special_report.json
```

Usage in CI:
```sh
# fails the test if exit code is higher than zero
docker run -d --name license-reporter -v ${PWD}:/reports registry.gitlab.com/comedian780/gitlab-license-reporter/license-reporter
docker logs -f license-reporter
exit $(docker inspect license-reporter --format='{{.State.ExitCode}}')
```
