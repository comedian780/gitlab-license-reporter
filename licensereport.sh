#!/bin/sh

print_green (){
      echo -e "\033[0;32m${1}\033[0m";
}

print_green "You use components with the following licenses";
jq -C -f jq_filters/show_licenses.jq "reports/${1:-gl-license-management-report.json}";

if [[ $(jq -f jq_filters/filter_copyleft.jq "reports/${1:-gl-license-management-report.json}" | wc -l) -ge 1 ]];
then
  print_green "The following licences have a copyleft";
  jq -C -f jq_filters/filter_copyleft.jq "reports/${1:-gl-license-management-report.json}"
  print_green "These are your installed packages";
  jq -C -f jq_filters/show_dependencies.jq "reports/${1:-gl-license-management-report.json}"
  exit 1;
else
  print_green "None of these licenses contian a copyleft.";
  print_green "You can use your code without distributing its sources.";
fi
